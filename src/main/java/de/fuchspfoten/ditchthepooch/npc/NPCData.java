package de.fuchspfoten.ditchthepooch.npc;

import lombok.Getter;
import org.bukkit.Location;
import org.bukkit.configuration.serialization.ConfigurationSerializable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * The data associated with an NPC.
 */
public class NPCData implements ConfigurationSerializable {

    /**
     * The ID of the NPC entity.
     */
    private @Getter final UUID id;

    /**
     * The skin key of the NPC.
     */
    private @Getter final String skinKey;

    /**
     * The trigger map of the NPC.
     */
    private final Map<String, List<String>> triggers;

    /**
     * The texts that the NPC uses.
     */
    private final Map<String, List<String>> texts;

    /**
     * The home location of the NPC entity.
     */
    private Location homeLocation;

    /**
     * The name of the NPC.
     */
    private @Getter String name;

    /**
     * Flag that indicates whether or not the data was changed.
     */
    private @Getter boolean dirty;

    /**
     * Constructor.
     *
     * @param id           The UUID of the NPC.
     * @param homeLocation The home location of the NPC.
     */
    public NPCData(final UUID id, final Location homeLocation) {
        this.id = id;
        this.homeLocation = homeLocation;
        name = "unbenannt";
        skinKey = "debug";
        triggers = new HashMap<>();
        texts = new HashMap<>();
        dirty = true;
    }

    /**
     * Deserialization constructor.
     *
     * @param source The source map.
     */
    @SuppressWarnings("unchecked")
    public NPCData(final Map<String, Object> source) {
        id = UUID.fromString((String) source.get("id"));
        homeLocation = (Location) source.get("home");
        name = (String) source.get("name");
        skinKey = (String) source.get("skin");
        triggers = (Map<String, List<String>>) source.getOrDefault("triggers", new HashMap<>());
        texts = (Map<String, List<String>>) source.getOrDefault("texts", new HashMap<>());
        dirty = false;
    }

    /**
     * Returns the UUID in version 2 format.
     *
     * @return The UUID.
     */
    public UUID getV2UUID() {
        final String uuidString = id.toString();
        final char[] uuidChars = uuidString.toCharArray();
        uuidChars[14] = '2';
        return UUID.fromString(String.valueOf(uuidChars));
    }

    /**
     * Returns a clone of the home location.
     *
     * @return The clone of the home location.
     */
    public Location getHomeLocation() {
        return homeLocation.clone();
    }

    /**
     * Setter for the home location.
     *
     * @param homeLocation The home location.
     */
    public void setHomeLocation(final Location homeLocation) {
        this.homeLocation = homeLocation;
        dirty = true;
    }

    /**
     * Setter for the NPC name.
     *
     * @param name The NPC name.
     */
    public void setName(final String name) {
        this.name = name;
        dirty = true;
    }

    /**
     * Returns the triggers registered in this NPC.
     *
     * @return The registered triggers.
     */
    public Collection<String> getTriggers() {
        return Collections.unmodifiableCollection(triggers.keySet());
    }

    /**
     * Returns the trigger script of the NPC for the given trigger.
     *
     * @param trigger The trigger.
     * @return The trigger script.
     */
    public List<String> getTriggerScript(final String trigger) {
        return Collections.unmodifiableList(triggers.getOrDefault(trigger, Collections.emptyList()));
    }

    /**
     * Sets the script for the given trigger. The trigger key must exist, otherwise, this method does nothing.
     *
     * @param trigger The trigger.
     * @param script  The script to set.
     */
    public void setTriggerScript(final String trigger, final List<String> script) {
        if (triggers.containsKey(trigger)) {
            triggers.put(trigger, new ArrayList<>(script));
            dirty = true;
        }
    }

    /**
     * Adds the given trigger. If the trigger exists, nothing happens.
     *
     * @param trigger The trigger.
     * @return {@code true} iff adding the trigger was successful.
     */
    public boolean addTrigger(final String trigger) {
        if (triggers.containsKey(trigger)) {
            return false;
        }
        triggers.put(trigger, new ArrayList<>());
        dirty = true;
        return true;
    }

    /**
     * Removes the given trigger. If the trigger does not exist, nothing happens.
     *
     * @param trigger The trigger.
     * @return {@code true} iff removing the trigger was successful.
     */
    public boolean removeTrigger(final String trigger) {
        if (!triggers.containsKey(trigger)) {
            return false;
        }
        triggers.remove(trigger);
        dirty = true;
        return true;
    }

    /**
     * Returns the text keys registered in this NPC.
     *
     * @return The registered text keys.
     */
    public Collection<String> getTextKeys() {
        return Collections.unmodifiableCollection(texts.keySet());
    }

    /**
     * Returns the texts for the given key.
     *
     * @param key The key.
     * @return The list of texts.
     */
    public List<String> getTextForKey(final String key) {
        return Collections.unmodifiableList(texts.getOrDefault(key, Collections.emptyList()));
    }

    /**
     * Sets the texts for the given key. The key must exist, otherwise, this method does nothing.
     *
     * @param key  The text key.
     * @param text The texts to set.
     */
    public void setTextForKey(final String key, final List<String> text) {
        if (texts.containsKey(key)) {
            texts.put(key, new ArrayList<>(text));
            dirty = true;
        }
    }

    /**
     * Adds the given text key. If the text key exists, nothing happens.
     *
     * @param key The text key.
     * @return {@code true} iff adding the key was successful.
     */
    public boolean addTextKey(final String key) {
        if (texts.containsKey(key)) {
            return false;
        }
        texts.put(key, new ArrayList<>());
        dirty = true;
        return true;
    }

    /**
     * Removes the given text key. If the text key does not exist, nothing happens.
     *
     * @param key The text key.
     * @return {@code true} iff removing the key was successful.
     */
    public boolean removeTextKey(final String key) {
        if (!texts.containsKey(key)) {
            return false;
        }
        texts.remove(key);
        dirty = true;
        return true;
    }

    @Override
    public Map<String, Object> serialize() {
        final Map<String, Object> result = new HashMap<>();
        result.put("id", id.toString());
        result.put("home", homeLocation);
        result.put("name", name);
        result.put("skin", skinKey);
        result.put("triggers", triggers);
        result.put("texts", texts);
        return result;
    }
}
