package de.fuchspfoten.ditchthepooch.npc;

import com.comphenix.packetwrapper.WrapperPlayServerEntityHeadRotation;
import com.comphenix.packetwrapper.WrapperPlayServerEntityLook;
import com.comphenix.packetwrapper.WrapperPlayServerEntityMetadata;
import com.comphenix.packetwrapper.WrapperPlayServerNamedEntitySpawn;
import com.comphenix.packetwrapper.WrapperPlayServerPlayerInfo;
import com.comphenix.packetwrapper.WrapperPlayServerSpawnEntityLiving;
import com.comphenix.protocol.PacketType.Play.Server;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.wrappers.EnumWrappers.NativeGameMode;
import com.comphenix.protocol.wrappers.EnumWrappers.PlayerInfoAction;
import com.comphenix.protocol.wrappers.PlayerInfoData;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import com.comphenix.protocol.wrappers.WrappedDataWatcher;
import com.comphenix.protocol.wrappers.WrappedDataWatcher.Registry;
import com.comphenix.protocol.wrappers.WrappedDataWatcher.WrappedDataWatcherObject;
import com.comphenix.protocol.wrappers.WrappedGameProfile;
import com.comphenix.protocol.wrappers.WrappedSignedProperty;
import com.comphenix.protocol.wrappers.WrappedWatchableObject;
import de.fuchspfoten.ditchthepooch.DitchThePoochPlugin;
import de.fuchspfoten.ditchthepooch.modules.ZombieNPCModule;
import de.fuchspfoten.skinrepository.SkinData;
import de.fuchspfoten.skinrepository.SkinRepositoryPlugin;
import net.minecraft.server.v1_12_R1.NBTTagCompound;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Converts zombie NPCs to human NPCs.
 */
public class NPCFilter extends PacketAdapter {

    /**
     * Creates a data watcher for an NPC.
     *
     * @param name The name of the NPC.
     * @return The data watcher.
     */
    private static WrappedDataWatcher createNPCWatcher(final String name) {
        final WrappedDataWatcher dataWatcher = new WrappedDataWatcher();

        // Entity status flags.
        dataWatcher.setObject(new WrappedDataWatcherObject(0, Registry.get(Byte.class)), (byte) 0);

        // Air.
        dataWatcher.setObject(new WrappedDataWatcherObject(1, Registry.get(Integer.class)), 300);

        // Custom name.
        dataWatcher.setObject(new WrappedDataWatcherObject(2, Registry.get(String.class)), name);

        // Custom name visibility.
        dataWatcher.setObject(new WrappedDataWatcherObject(3, Registry.get(Boolean.class)), true);

        // Silent entity?
        dataWatcher.setObject(new WrappedDataWatcherObject(4, Registry.get(Boolean.class)), true);

        // No gravity?
        dataWatcher.setObject(new WrappedDataWatcherObject(5, Registry.get(Boolean.class)), false);

        // Hand state.
        dataWatcher.setObject(new WrappedDataWatcherObject(6, Registry.get(Byte.class)), (byte) 0);

        // Health.
        dataWatcher.setObject(new WrappedDataWatcherObject(7, Registry.get(Float.class)), 20.0f);

        // Potion effect color.
        dataWatcher.setObject(new WrappedDataWatcherObject(8, Registry.get(Integer.class)), 0);

        // Potion effect ambient?
        dataWatcher.setObject(new WrappedDataWatcherObject(9, Registry.get(Boolean.class)), false);

        // Number of arrows in entity.
        dataWatcher.setObject(new WrappedDataWatcherObject(10, Registry.get(Integer.class)), 0);

        // Additional hearts.
        dataWatcher.setObject(new WrappedDataWatcherObject(11, Registry.get(Float.class)), 0.0f);

        // Score.
        dataWatcher.setObject(new WrappedDataWatcherObject(12, Registry.get(Integer.class)), 0);

        // Displayed skin bits.
        dataWatcher.setObject(new WrappedDataWatcherObject(13, Registry.get(Byte.class)), (byte) 127);

        // Main hand.
        dataWatcher.setObject(new WrappedDataWatcherObject(14, Registry.get(Byte.class)), (byte) 1);

        // Left shoulder.
        dataWatcher.setObject(new WrappedDataWatcherObject(15, Registry.getNBTCompoundSerializer()),
                Unsafe.newNBTCompound());

        // Right shoulder.
        dataWatcher.setObject(new WrappedDataWatcherObject(16, Registry.getNBTCompoundSerializer()),
                Unsafe.newNBTCompound());

        return dataWatcher;
    }

    /**
     * Sends a packet to modify the player info entry for the given game profile.
     *
     * @param recipient The recipient.
     * @param profile   The game profile.
     * @param action    The action to perform.
     */
    private static void modifyPlayerInfo(final Player recipient, final WrappedGameProfile profile,
                                         final PlayerInfoAction action) {
        final WrapperPlayServerPlayerInfo infoRemovePacket = new WrapperPlayServerPlayerInfo();
        infoRemovePacket.setAction(action);
        infoRemovePacket.setData(Collections.singletonList(
                new PlayerInfoData(
                        profile,
                        20,
                        NativeGameMode.SURVIVAL,
                        WrappedChatComponent.fromText("NPC")
                )
        ));
        infoRemovePacket.sendPacket(recipient);
    }

    /**
     * The zombie NPC module.
     */
    private final ZombieNPCModule module;

    /**
     * Constructor.
     */
    public NPCFilter(final ZombieNPCModule module) {
        super(DitchThePoochPlugin.getSelf(), ListenerPriority.NORMAL, Server.SPAWN_ENTITY_LIVING,
                Server.ENTITY_METADATA, Server.ENTITY_LOOK, Server.ENTITY_HEAD_ROTATION);
        this.module = module;
    }

    /**
     * Handles {@link Server#ENTITY_METADATA}.
     *
     * @param event The event.
     */
    private void handleEntityMetadata(final PacketEvent event) {
        final WrapperPlayServerEntityMetadata packet = new WrapperPlayServerEntityMetadata(event.getPacket());
        final NPC npc = module.getNPC(packet.getEntityID());
        if (npc == null) {
            return;
        }

        // Filter data watcher entries above 10 (these would concern players, but are sent from a zombie).
        final List<WrappedWatchableObject> filtered = new ArrayList<>();
        packet.getMetadata().stream().filter(entry -> entry.getIndex() <= 10).forEach(filtered::add);
        packet.setMetadata(filtered);
    }

    /**
     * Handles {@link Server#ENTITY_LOOK}.
     *
     * @param event The event.
     */
    private void handleEntityLook(final PacketEvent event) {
        final WrapperPlayServerEntityLook packet = new WrapperPlayServerEntityLook(event.getPacket());
        final NPC npc = module.getNPC(packet.getEntityID());
        if (npc == null) {
            return;
        }

        // Update the yaw of the entity look packet (players are handled differently than living beings by the client).
        packet.setYaw(npc.getBukkitEntity().getLocation().getYaw());
    }

    /**
     * Handles {@link Server#ENTITY_HEAD_ROTATION}.
     *
     * @param event The event.
     */
    private void handleEntityHeadRotation(final PacketEvent event) {
        final WrapperPlayServerEntityHeadRotation packet = new WrapperPlayServerEntityHeadRotation(event.getPacket());
        final NPC npc = module.getNPC(packet.getEntityID());
        if (npc == null) {
            return;
        }

        // Send an accompanying look packet.
        final WrapperPlayServerEntityLook tagAlong = new WrapperPlayServerEntityLook();
        tagAlong.setEntityID(packet.getEntityID());
        tagAlong.setOnGround(npc.getBukkitEntity().isOnGround());
        tagAlong.setPitch(npc.getBukkitEntity().getLocation().getPitch());
        tagAlong.setYaw(0);  // Yaw is updated anyways, so no need to set it here.
        tagAlong.sendPacket(event.getPlayer());
    }

    /**
     * Handles {@link Server#SPAWN_ENTITY_LIVING}.
     *
     * @param event The event.
     */
    private void handleSpawnEntityLiving(final PacketEvent event) {
        final WrapperPlayServerSpawnEntityLiving oldPacket = new WrapperPlayServerSpawnEntityLiving(event.getPacket());
        final NPC npc = module.getNPC(oldPacket.getEntityID());
        if (npc == null) {
            return;
        }
        event.setCancelled(true);

        // Create the game profile for the NPC.
        final SkinData skinData = SkinRepositoryPlugin.getSelf().getSkinData(npc.getData().getSkinKey());
        final WrappedGameProfile gameProfile = new WrappedGameProfile(npc.getData().getV2UUID(),
                npc.getData().getName());
        gameProfile.getProperties().put("textures",
                new WrappedSignedProperty("textures", skinData.getValue(), skinData.getSignature()));

        // Add the player info.
        modifyPlayerInfo(event.getPlayer(), gameProfile, PlayerInfoAction.ADD_PLAYER);

        // Create and send the spawn packet.
        final WrapperPlayServerNamedEntitySpawn spawnPacket = new WrapperPlayServerNamedEntitySpawn();
        spawnPacket.setEntityID(oldPacket.getEntityID());
        spawnPacket.setPlayerUUID(npc.getData().getV2UUID());  // According to wiki.vg, we use V2 for NPCs.
        spawnPacket.setX(oldPacket.getX());
        spawnPacket.setY(oldPacket.getY());
        spawnPacket.setZ(oldPacket.getZ());
        spawnPacket.setYaw(oldPacket.getYaw());
        spawnPacket.setPitch(oldPacket.getPitch());
        spawnPacket.setMetadata(createNPCWatcher(npc.getData().getName()));
        spawnPacket.sendPacket(event.getPlayer());

        // Remove the player info.
        Bukkit.getScheduler().scheduleSyncDelayedTask(
                DitchThePoochPlugin.getSelf(),
                () -> modifyPlayerInfo(event.getPlayer(), gameProfile, PlayerInfoAction.REMOVE_PLAYER),
                1L
        );

        // Update position and look.
        final WrapperPlayServerEntityHeadRotation headPacket = new WrapperPlayServerEntityHeadRotation();
        headPacket.setEntityID(oldPacket.getEntityID());
        final float yaw = npc.getBukkitEntity().getLocation().getYaw();
        headPacket.setHeadYaw((byte) (256 * (yaw / 360.0f)));
        headPacket.sendPacket(event.getPlayer());
    }

    @Override
    public void onPacketSending(final PacketEvent event) {
        if (event.getPacketType() == Server.ENTITY_METADATA) {
            handleEntityMetadata(event);
        } else if (event.getPacketType() == Server.SPAWN_ENTITY_LIVING) {
            handleSpawnEntityLiving(event);
        } else if (event.getPacketType() == Server.ENTITY_LOOK) {
            handleEntityLook(event);
        } else if (event.getPacketType() == Server.ENTITY_HEAD_ROTATION) {
            handleEntityHeadRotation(event);
        }
    }

    /**
     * Provides unsafe operations.
     */
    private static final class Unsafe {

        /**
         * Creates an empty NBT tag compound.
         *
         * @return The empty compound.
         */
        public static Object newNBTCompound() {
            return new NBTTagCompound();
        }
    }
}
