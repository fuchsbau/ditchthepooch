package de.fuchspfoten.ditchthepooch.npc.script;

import lombok.Builder;
import lombok.Getter;
import org.bukkit.entity.Player;

/**
 * Represents the context for a trigger call.
 */
@Builder
public class TriggerContext {

    /**
     * The target player, if any.
     */
    private @Getter final Player targetPlayer;
}
