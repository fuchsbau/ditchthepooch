package de.fuchspfoten.ditchthepooch.npc.script;

import de.fuchspfoten.ditchthepooch.DitchThePoochPlugin;
import de.fuchspfoten.ditchthepooch.npc.NPC;
import de.fuchspfoten.ditchthepooch.npc.script.insn.TalkPickInstruction;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * The script engine for trigger scripts.
 */
public class ScriptEngine {

    /**
     * Cache for instructions. The complete instruction line is used as a key.
     */
    private final Map<String, Instruction> instructionCache = new HashMap<>();

    /**
     * The registry for instruction creation.
     */
    private final Map<String, Function<String, Instruction>> registry = new HashMap<>();

    /**
     * Constructor.
     */
    public ScriptEngine() {
        registry.put("talk.pick", TalkPickInstruction::new);
    }

    /**
     * Registers an instruction with the NPC script engine.
     *
     * @param mnemonic The mnemonic of the instruction.
     * @param provider The instruction provider.
     */
    public void registerInstruction(final String mnemonic, final Function<String, Instruction> provider) {
        registry.put(mnemonic, provider);
    }

    /**
     * Executes the given line for the given NPC in the given context.
     *
     * @param line    The line.
     * @param npc     The NPC.
     * @param context The context.
     */
    public void execute(final String line, final NPC npc, final TriggerContext context) {
        final Instruction cached = instructionCache.get(line);
        if (cached != null) {
            cached.execute(npc, context);
        } else {
            final String[] parts = line.split(" ", 2);
            final Function<String, Instruction> instructionProvider = registry.get(parts[0]);
            if (instructionProvider == null) {
                DitchThePoochPlugin.getSelf().getLogger().severe("Invalid instruction: " + line);
                return;
            }

            // Cache and execute the instruction.
            final Instruction insn = instructionProvider.apply(parts.length > 1 ? parts[1] : "");
            instructionCache.put(line, insn);
            insn.execute(npc, context);
        }
    }
}
