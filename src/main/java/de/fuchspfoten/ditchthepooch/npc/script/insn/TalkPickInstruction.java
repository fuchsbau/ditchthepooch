package de.fuchspfoten.ditchthepooch.npc.script.insn;

import de.fuchspfoten.ditchthepooch.DitchThePoochPlugin;
import de.fuchspfoten.ditchthepooch.npc.NPC;
import de.fuchspfoten.ditchthepooch.npc.script.Instruction;
import de.fuchspfoten.ditchthepooch.npc.script.TriggerContext;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Random;

/**
 * Picks a message from a text key and sends it via the NPC.
 */
@RequiredArgsConstructor
public class TalkPickInstruction implements Instruction {

    /**
     * The random number generator.
     */
    private static final Random random = new Random();

    /**
     * The text key to pick from.
     */
    private final String textKey;

    @Override
    public void execute(final NPC npc, final TriggerContext context) {
        if (context.getTargetPlayer() == null) {
            DitchThePoochPlugin.getSelf().getLogger().severe("TalkPickInstruction without target player");
            return;
        }

        final List<String> text = npc.getData().getTextForKey(textKey);
        if (!text.isEmpty()) {
            final String pick = text.get(random.nextInt(text.size()));
            npc.talk(context.getTargetPlayer(), pick);
        }
    }
}
