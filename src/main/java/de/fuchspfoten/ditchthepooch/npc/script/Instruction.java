package de.fuchspfoten.ditchthepooch.npc.script;

import de.fuchspfoten.ditchthepooch.npc.NPC;

/**
 * Represents an instruction that can be run. Instructions should be stateless.
 */
public interface Instruction {

    /**
     * Executes the instruction.
     *
     * @param npc     The NPC to execute the instruction for.
     * @param context The context in which the instruction is executed.
     */
    void execute(NPC npc, TriggerContext context);
}
