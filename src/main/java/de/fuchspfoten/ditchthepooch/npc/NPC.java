package de.fuchspfoten.ditchthepooch.npc;

import de.fuchspfoten.ditchthepooch.DitchThePoochPlugin;
import de.fuchspfoten.ditchthepooch.npc.ai.AIGoalPreferHome;
import de.fuchspfoten.ditchthepooch.npc.ai.AIGoalReturnHome;
import de.fuchspfoten.ditchthepooch.npc.script.ScriptEngine;
import de.fuchspfoten.ditchthepooch.npc.script.TriggerContext;
import de.fuchspfoten.fuchslib.Messenger;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Zombie;
import org.bukkit.metadata.FixedMetadataValue;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Represents an NPC.
 */
@RequiredArgsConstructor
public class NPC {

    /**
     * A weak reference to the bukkit entity for this NPC.
     */
    private final WeakReference<Zombie> bukkitEntity;

    /**
     * The NPC data storage for this NPC.
     */
    private @Getter final NPCData data;

    /**
     * Checks whether this NPC is present, i.e. loaded and linked to a bukkit entity.
     *
     * @return {@code true} iff this NPC is present.
     */
    public boolean isPresent() {
        return bukkitEntity.get() != null;
    }

    /**
     * Returns the bukkit entity for this NPC.
     *
     * @return The bukkit entity.
     */
    public Zombie getBukkitEntity() {
        return bukkitEntity.get();
    }

    /**
     * Initializes the underlying bukkit entity.
     */
    public void initializeEntity() {
        // Initialize the entity's AI exactly once by using non-persistent metadata.
        if (!getBukkitEntity().hasMetadata("ZombieNPC_Init")) {
            getBukkitEntity().setMetadata("ZombieNPC_Init",
                    new FixedMetadataValue(DitchThePoochPlugin.getSelf(), 1));
            getBukkitEntity().setRemoveWhenFarAway(false);
            getBukkitEntity().setSilent(true);

            // Initialize AI for the NPC.
            AIController.clearBehaviors(getBukkitEntity());
            AIController.addBaseBehaviors(getBukkitEntity());
            AIController.addAIGoal(getBukkitEntity(), 4, new AIGoalReturnHome(this));
            AIController.addAIGoal(getBukkitEntity(), 6, new AIGoalPreferHome(this));
        }
    }

    /**
     * Checks whether the NPC has left its home radius.
     *
     * @return {@code true} iff the NPC left its home radius.
     */
    public boolean hasLeftHomeRadius() {
        final double distanceSq = getBukkitEntity().getLocation().distanceSquared(data.getHomeLocation());
        return distanceSq > 8 * 8;
    }

    /**
     * Runs the trigger with the given name.
     *
     * @param trigger The trigger name.
     * @param context The trigger context.
     */
    public void runTrigger(final String trigger, final TriggerContext context) {
        final List<String> script = data.getTriggerScript(trigger);
        final ScriptEngine engine = DitchThePoochPlugin.getSelf().getZombieNPCModule().getScriptEngine();
        for (final String insnLine : script) {
            engine.execute(insnLine, this, context);
        }
    }

    /**
     * Makes the NPC talk to the given sender.
     *
     * @param sender The sender.
     * @param text   The text to say.
     */
    public void talk(final CommandSender sender, final String text) {
        Messenger.send(sender, "ditchthepooch.npc.talk", ChatColor.stripColor(data.getName()), text);
    }
}
