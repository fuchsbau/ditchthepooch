package de.fuchspfoten.ditchthepooch.npc;

import de.fuchspfoten.ditchthepooch.npc.ai.AIGoal;
import de.fuchspfoten.fuchslib.util.ReflectionHelper;
import net.minecraft.server.v1_12_R1.EntityHuman;
import net.minecraft.server.v1_12_R1.EntityInsentient;
import net.minecraft.server.v1_12_R1.EntityZombie;
import net.minecraft.server.v1_12_R1.PathfinderGoal;
import net.minecraft.server.v1_12_R1.PathfinderGoalFloat;
import net.minecraft.server.v1_12_R1.PathfinderGoalLookAtPlayer;
import net.minecraft.server.v1_12_R1.PathfinderGoalRandomLookaround;
import net.minecraft.server.v1_12_R1.PathfinderGoalSelector;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftEntity;
import org.bukkit.entity.LivingEntity;

import java.util.Collection;

/**
 * Provides utility methods for controlling AI.
 */
public final class AIController {

    /**
     * Clears the behaviors for the given entity.
     *
     * @param entity The entity to clear behaviors for.
     */
    public static void clearBehaviors(final LivingEntity entity) {
        Unsafe.clearGoalSelector(entity);
        Unsafe.clearTargetSelector(entity);
    }

    /**
     * Adds basic NPC behaviors for the given entity.
     *
     * @param entity The entity to add NPC behaviors for.
     */
    public static void addBaseBehaviors(final LivingEntity entity) {
        Unsafe.addBaseBehaviors(entity);
    }

    /**
     * Adds an AI goal for the given entity.
     *
     * @param entity   The entity.
     * @param priority The priority for the AI goal.
     * @param goal     The AI goal.
     */
    public static void addAIGoal(final LivingEntity entity, final int priority, final AIGoal goal) {
        Unsafe.addAIGoal(entity, priority, goal);
    }

    /**
     * Private constructor to prevent instance creation.
     */
    private AIController() {
    }

    /**
     * Provides unsafe operations.
     */
    private static final class Unsafe {

        /**
         * Clears the goal selector for the given entity.
         *
         * @param entity The entity.
         */
        public static void clearGoalSelector(final LivingEntity entity) {
            ((Collection<?>) ReflectionHelper.getPrivateField(PathfinderGoalSelector.class, "b",
                    getHandle(entity).goalSelector)).clear();
        }

        /**
         * Adds basic NPC behaviors for the given entity.
         *
         * @param entity The entity.
         */
        public static void addBaseBehaviors(final LivingEntity entity) {
            final EntityInsentient handle = getHandle(entity);
            handle.goalSelector.a(0, new PathfinderGoalFloat(handle));
            handle.goalSelector.a(7, new PathfinderGoalLookAtPlayer(handle, EntityHuman.class, 8.0F));
            handle.goalSelector.a(8, new PathfinderGoalLookAtPlayer(handle, EntityZombie.class, 8.0F));
            handle.goalSelector.a(9, new PathfinderGoalRandomLookaround(handle));
        }

        /**
         * Adds an AI goal for the given entity.
         *
         * @param entity   The entity.
         * @param priority The priority for the AI goal.
         * @param goal     The AI goal.
         */
        public static void addAIGoal(final LivingEntity entity, final int priority, final AIGoal goal) {
            final EntityInsentient handle = getHandle(entity);
            handle.goalSelector.a(priority, (PathfinderGoal) goal.getHandle());
        }

        /**
         * Clears the target selector for the given entity.
         *
         * @param entity The entity.
         */
        public static void clearTargetSelector(final LivingEntity entity) {
            ((Collection<?>) ReflectionHelper.getPrivateField(PathfinderGoalSelector.class, "b",
                    getHandle(entity).targetSelector)).clear();
        }

        /**
         * Obtains the goal and target selecting handle for the given entity.
         *
         * @param entity The entity.
         * @return The handle.
         */
        private static EntityInsentient getHandle(final LivingEntity entity) {
            return (EntityInsentient) ((CraftEntity) entity).getHandle();
        }
    }
}
