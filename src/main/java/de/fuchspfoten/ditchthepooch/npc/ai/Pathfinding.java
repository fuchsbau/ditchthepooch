package de.fuchspfoten.ditchthepooch.npc.ai;

import net.minecraft.server.v1_12_R1.EntityCreature;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftCreature;
import org.bukkit.entity.Creature;

/**
 * Provides utilities for pathfinding.
 */
public final class Pathfinding {

    /**
     * Checks whether the given creature is currently pathfinding.
     *
     * @param creature The creature.
     * @return {@code true} iff the creature is currently pathfinding.
     */
    public static boolean isPathfinding(final Creature creature) {
        return Unsafe.isPathfinding(creature);
    }

    /**
     * Attempts to pathfind to the given location.
     *
     * @param creature The creature.
     * @param location The location.
     * @return {@code true} iff successful.
     */
    public static boolean moveToLocation(final Creature creature, final Location location) {
        return Unsafe.moveToLocation(creature, location.getX(), location.getY(), location.getZ(), 1.0);
    }

    /**
     * Private constructor to prevent instance creation.
     */
    private Pathfinding() {
    }

    /**
     * Provides unsafe operations.
     */
    private static final class Unsafe {

        /**
         * Checks whether the given creature is currently pathfinding.
         *
         * @param creature The creature.
         * @return {@code true} iff the creature is currently pathfinding.
         */
        public static boolean isPathfinding(final Creature creature) {
            return !getHandle(creature).getNavigation().o();
        }

        /**
         * Attempts to pathfind to the given location.
         *
         * @param creature The creature.
         * @param x        The x position.
         * @param y        The y position.
         * @param z        The z position.
         * @param speed    The movement speed.
         * @return {@code true} iff successful.
         */
        public static boolean moveToLocation(final Creature creature, final double x, final double y, final double z,
                                             final double speed) {
            return getHandle(creature).getNavigation().a(x, y, z, speed);
        }

        /**
         * Returns the NMS handle of the given creature.
         *
         * @param creature The creature.
         * @return The NMS handle.
         */
        private static EntityCreature getHandle(final Creature creature) {
            return ((CraftCreature) creature).getHandle();
        }
    }
}
