package de.fuchspfoten.ditchthepooch.npc.ai;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.server.v1_12_R1.PathfinderGoal;

/**
 * Represents an AI goal.
 */
public abstract class AIGoal {

    /**
     * A bitmask used for checking if goals may be ran concurrently -- intersection is forbidden.
     */
    private @Getter @Setter int mutexBits;

    /**
     * Returns {@code true} if the goal should be executed.
     */
    public abstract boolean shouldExecute();

    /**
     * Returns {@code true} if the goal should be kept executing.
     */
    public boolean shouldContinueExecuting() {
        return shouldExecute();
    }

    /**
     * If this is {@code true}, a higher priority task may interrupt this task.
     */
    public boolean isInterruptible() {
        return true;
    }

    /**
     * Callback for beginning the execution of a task.
     */
    public void startExecuting() {
    }

    /**
     * Called when the task is interrupted. Is used to reset the internal state.
     */
    public void resetTask() {
    }

    /**
     * Callback for continuing the execution of a task.
     */
    public void updateTask() {
    }

    /**
     * Obtains a native handle for this task.
     *
     * @return The native handle.
     */
    public Object getHandle() {
        return Unsafe.asNMSTask(this);
    }

    /**
     * Contains unsafe operations.
     */
    private static class Unsafe {

        /**
         * Returns an NMS task for the given task.
         *
         * @param goal The task to convert.
         * @return The NMS task.
         */
        private static PathfinderGoal asNMSTask(final AIGoal goal) {
            // Suppressing these as we do not want to create an unsafe inner class.
            //noinspection InnerClassTooDeeplyNested,AnonymousInnerClassWithTooManyMethods
            return new PathfinderGoal() {
                @Override
                public boolean a() {
                    return goal.shouldExecute();
                }

                @Override
                public boolean b() {
                    return goal.shouldContinueExecuting();
                }

                @Override
                public boolean g() {
                    return goal.isInterruptible();
                }

                @Override
                public void c() {
                    goal.startExecuting();
                }

                @Override
                public void d() {
                    goal.resetTask();
                }

                @Override
                public void e() {
                    goal.updateTask();
                }

                @Override
                public void a(final int i) {
                    goal.setMutexBits(i);
                }

                @Override
                public int h() {
                    return goal.getMutexBits();
                }
            };
        }
    }
}