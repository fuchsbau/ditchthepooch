package de.fuchspfoten.ditchthepooch.npc.ai;

import de.fuchspfoten.ditchthepooch.npc.NPC;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Zombie;

/**
 * This AI causes an entity to return to its home location.
 */
public class AIGoalReturnHome extends AIGoal {

    /**
     * The NPC this behavior is assigned to.
     */
    private final NPC npc;

    /**
     * If this is {@code true}, the NPC uses magic to return to its home location (path obstructed).
     */
    private boolean useMagic;

    /**
     * When this reaches 0, the NPC is teleported to its home location.
     */
    private long teleportTimer;

    /**
     * Counts pathfinding failures.
     */
    private int pathfindFailures;

    /**
     * Constructor.
     *
     * @param npc The NPC.
     */
    public AIGoalReturnHome(final NPC npc) {
        this.npc = npc;
        setMutexBits(1);
    }

    @Override
    public boolean shouldExecute() {
        return npc.hasLeftHomeRadius();
    }

    @Override
    public boolean shouldContinueExecuting() {
        if (useMagic) {
            return npc.hasLeftHomeRadius();
        }
        return Pathfinding.isPathfinding(npc.getBukkitEntity());
    }

    @Override
    public void startExecuting() {
        if (npc.getData().getHomeLocation().distanceSquared(npc.getBukkitEntity().getLocation()) > 24 * 24) {
            useMagic = true;
            teleportTimer = 200;
        } else {
            if (Pathfinding.moveToLocation(npc.getBukkitEntity(), npc.getData().getHomeLocation())) {
                pathfindFailures = 0;
            } else {
                if (pathfindFailures++ > 16) {
                    useMagic = true;
                    teleportTimer = 200;
                }
            }
        }
    }

    @Override
    public void resetTask() {
        // Do not reset pathfind failures: Count across runs.
        useMagic = false;
    }

    @Override
    public void updateTask() {
        if (useMagic) {
            --teleportTimer;
            final Zombie entity = npc.getBukkitEntity();
            final World world = entity.getWorld();
            final Location origin = entity.getEyeLocation().subtract(0, 0.5 * entity.getEyeHeight(), 0);
            final Location homeOrigin = npc.getData().getHomeLocation().add(0, 0.5 * entity.getEyeHeight(), 0);
            if (teleportTimer == 0) {
                entity.teleport(npc.getData().getHomeLocation());
                world.playSound(origin, Sound.ENTITY_ENDERDRAGON_HURT, 0.3f, 0.3f);
            } else {
                if (teleportTimer <= 180 && teleportTimer > 60) {
                    final int count = (int) (((180 - teleportTimer) / 180.0f) * 16);
                    world.spawnParticle(Particle.SPELL_WITCH, origin, count, 0.7, 0.7, 0.7);
                } else if (teleportTimer <= 60) {
                    world.spawnParticle(Particle.EXPLOSION_LARGE, origin, 1, 0.5, 0.5,
                            0.5);
                    world.spawnParticle(Particle.EXPLOSION_LARGE, homeOrigin, 1, 0.5, 0.5,
                            0.5);
                }
                if (teleportTimer % 10 == 0) {
                    world.playSound(origin, Sound.BLOCK_PORTAL_TRAVEL, 0.3f,
                            (180 - teleportTimer) / 90.0f);
                }
            }
        }
    }
}
