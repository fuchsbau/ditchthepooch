package de.fuchspfoten.ditchthepooch.npc.ai;

import de.fuchspfoten.ditchthepooch.npc.NPC;

import java.util.Random;

/**
 * This AI causes an entity to return to its home location.
 */
public class AIGoalPreferHome extends AIGoal {

    /**
     * The random number generator.
     */
    private static final Random random = new Random();

    /**
     * The NPC this behavior is assigned to.
     */
    private final NPC npc;

    /**
     * Counts pathfinding failures.
     */
    private int pathfindFailures;

    /**
     * Constructor.
     *
     * @param npc The NPC.
     */
    public AIGoalPreferHome(final NPC npc) {
        this.npc = npc;
        setMutexBits(1);
    }

    @Override
    public boolean shouldExecute() {
        return random.nextDouble() < 0.02
                && npc.getData().getHomeLocation().distanceSquared(npc.getBukkitEntity().getLocation()) > 1;
    }

    @Override
    public boolean shouldContinueExecuting() {
        return Pathfinding.isPathfinding(npc.getBukkitEntity());
    }

    @Override
    public void startExecuting() {
        if (Pathfinding.moveToLocation(npc.getBukkitEntity(), npc.getData().getHomeLocation())) {
            pathfindFailures = 0;
        } else {
            if (pathfindFailures++ > 12) {
                npc.getBukkitEntity().teleport(npc.getData().getHomeLocation());
            }
        }
    }
}
