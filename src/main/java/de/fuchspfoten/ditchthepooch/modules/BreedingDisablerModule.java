package de.fuchspfoten.ditchthepooch.modules;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.entity.EntityBreedEvent;

/**
 * Disables breeding of entities.
 */
public class BreedingDisablerModule implements Listener {

    @EventHandler
    public void onEntityBreed(final EntityBreedEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onCreatureSpawn(final CreatureSpawnEvent event) {
        // For chickens.
        if (event.getSpawnReason() == SpawnReason.EGG) {
            event.setCancelled(true);
        }
    }
}
