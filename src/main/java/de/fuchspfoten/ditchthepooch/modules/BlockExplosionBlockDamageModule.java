package de.fuchspfoten.ditchthepooch.modules;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.entity.EntityExplodeEvent;

/**
 * Blocks explosion block damage (controlling explosions as entities -> this plugin).
 */
public class BlockExplosionBlockDamageModule implements Listener {

    @EventHandler
    public void onEntityExplode(final EntityExplodeEvent event) {
        event.blockList().clear();
    }

    @EventHandler
    public void onBlockExplode(final BlockExplodeEvent event) {
        event.blockList().clear();
    }
}
