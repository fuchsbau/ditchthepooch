package de.fuchspfoten.ditchthepooch.modules;

import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.entity.EntityBreedEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

/**
 * Makes villagers invincible and unbreedable.
 */
public class VillagerInvincibilityModule implements Listener {

    @EventHandler
    public void onEntityDamage(final EntityDamageEvent event) {
        // Allow void and cramming damage.
        switch (event.getCause()) {
            case VOID:
            case CRAMMING:
                return;
            default:
                break;
        }

        if (event.getEntity() instanceof Villager) {
            event.setCancelled(true);
        }

        if (event instanceof EntityDamageByEntityEvent) {
            final EntityDamageByEntityEvent specEvent = (EntityDamageByEntityEvent) event;
            if (specEvent.getDamager().isOp()) {
                event.setCancelled(false);
            }
        }
    }

    @EventHandler
    public void onEntityBreed(final EntityBreedEvent event) {
        if (event.getEntity() instanceof Villager) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onCreatureSpawn(final CreatureSpawnEvent event) {
        if (event.getEntity() instanceof Villager) {
            if (event.getSpawnReason() == SpawnReason.CURED) {
                event.setCancelled(true);
            }
        }
    }
}
