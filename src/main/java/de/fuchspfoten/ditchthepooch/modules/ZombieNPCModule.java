package de.fuchspfoten.ditchthepooch.modules;

import de.fuchspfoten.ditchthepooch.DitchThePoochPlugin;
import de.fuchspfoten.ditchthepooch.NPCVersionUpdater;
import de.fuchspfoten.ditchthepooch.npc.NPC;
import de.fuchspfoten.ditchthepooch.npc.NPCData;
import de.fuchspfoten.ditchthepooch.npc.script.ScriptEngine;
import de.fuchspfoten.ditchthepooch.npc.script.TriggerContext;
import de.fuchspfoten.fuchslib.data.DataFile;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Tameable;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityCombustByBlockEvent;
import org.bukkit.event.entity.EntityCombustByEntityEvent;
import org.bukkit.event.entity.EntityCombustEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.inventory.EquipmentSlot;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

/**
 * Module managing disguised zombie NPCs.
 */
public class ZombieNPCModule implements Listener {

    /**
     * The directory for NPC data.
     */
    private final File npcDirectory;

    /**
     * The data version updater.
     */
    private final NPCVersionUpdater versionUpdater;

    /**
     * The NPC data file storage. All existing NPCs are always present.
     */
    private final Map<UUID, DataFile> npcDataFileStorage = new HashMap<>();

    /**
     * The NPC data storage. All existing NPCs are always present.
     */
    private final Map<UUID, NPCData> npcDataStorage = new HashMap<>();

    /**
     * The NPC storage. NPCs may be not present.
     */
    private final Map<UUID, NPC> npcStorage = new HashMap<>();

    /**
     * Maps entity IDs of NPC entities to unique IDs. NPCs may be not present.
     */
    private final Map<Integer, UUID> npcEntityIdMap = new HashMap<>();

    /**
     * The trigger script script engine.
     */
    private @Getter final ScriptEngine scriptEngine = new ScriptEngine();

    /**
     * Constructor.
     *
     * @param npcDirectory The npc directory.
     */
    public ZombieNPCModule(final File npcDirectory) {
        this.npcDirectory = npcDirectory;
        versionUpdater = new NPCVersionUpdater();
        final File[] dataFiles = npcDirectory.listFiles();
        if (dataFiles != null) {
            for (final File file : dataFiles) {
                final DataFile dataFile = new DataFile(file, versionUpdater);
                final NPCData data = dataFile.getStorage().getSerializable("npc", NPCData.class);
                npcDataFileStorage.put(data.getId(), dataFile);
                npcDataStorage.put(data.getId(), data);
            }
        }

        // Scrape already loaded NPCs and schedule a task that will scrape every 30 seconds.
        scrapeAndInitializeAll();
        Bukkit.getScheduler().scheduleSyncRepeatingTask(DitchThePoochPlugin.getSelf(), this::scrapeAndInitializeAll,
                30 * 20L, 30 * 20L);
    }

    /**
     * Saves NPC data.
     */
    public void save() {
        for (final Entry<UUID, NPCData> entry : npcDataStorage.entrySet()) {
            if (entry.getValue().isDirty()) {
                final DataFile dataFile = npcDataFileStorage.get(entry.getKey());
                dataFile.getStorage().set("npc", entry.getValue());
                dataFile.save(NPCVersionUpdater.VERSION);
            }
        }
    }

    /**
     * Adds an NPC.
     *
     * @param zombie The zombie to add as an NPC.
     */
    public void addNPC(final Zombie zombie) {
        npcDataStorage.put(zombie.getUniqueId(), new NPCData(zombie.getUniqueId(), zombie.getLocation()));
        npcDataFileStorage.put(zombie.getUniqueId(),
                new DataFile(new File(npcDirectory, zombie.getUniqueId() + ".yml"), versionUpdater));
        initializeNPC(zombie);
    }

    @EventHandler
    public void onChunkLoad(final ChunkLoadEvent event) {
        for (final Entity entity : event.getChunk().getEntities()) {
            if (entity instanceof Zombie && isNPC(entity)) {
                initializeNPC((Zombie) entity);
            }
        }
    }

    @EventHandler
    public void onEntityDamage(final EntityDamageEvent event) {
        if (!(event.getEntity() instanceof Zombie)) {
            return;
        }

        if (!isNPC(event.getEntity())) {
            return;
        }

        // NPCs are invincible, but damageable.
        final Zombie zombie = (Zombie) event.getEntity();
        zombie.setHealth(zombie.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue());
        event.setDamage(0.1);
        zombie.getWorld().playSound(zombie.getLocation(), Sound.ENTITY_PLAYER_HURT, 0.5f, 1.0f);

        if (event instanceof EntityDamageByEntityEvent) {
            final NPC npc = getNPC(zombie.getUniqueId());
            final EntityDamageByEntityEvent entityEvent = (EntityDamageByEntityEvent) event;

            // Determine the damager.
            final Player damager;
            if (entityEvent.getDamager() instanceof Player) {
                damager = (Player) entityEvent.getDamager();
            } else if (entityEvent.getDamager() instanceof Projectile) {
                if (((Projectile) entityEvent.getDamager()).getShooter() instanceof Player) {
                    damager = (Player) ((Projectile) entityEvent.getDamager()).getShooter();
                } else {
                    damager = null;
                }
            } else if (entityEvent.getDamager() instanceof Tameable) {
                if (((Tameable) entityEvent.getDamager()).isTamed()
                        && ((Tameable) entityEvent.getDamager()).getOwner() instanceof Player) {
                    damager = (Player) ((Tameable) entityEvent.getDamager()).getOwner();
                } else {
                    damager = null;
                }
            } else {
                damager = null;
            }

            if (damager != null) {
                npc.runTrigger("hurtByPlayer", TriggerContext.builder().targetPlayer(damager).build());
            }
        }
    }

    @EventHandler
    public void onPlayerInteractEntity(final PlayerInteractEntityEvent event) {
        if (event.getHand() != EquipmentSlot.HAND) {
            return;
        }

        if (event.getRightClicked() instanceof Zombie) {
            if (isNPC(event.getRightClicked())) {
                final NPC npc = getNPC(event.getRightClicked().getUniqueId());
                npc.runTrigger("playerInteract",
                        TriggerContext.builder().targetPlayer(event.getPlayer()).build());
            }
        }
    }

    @EventHandler
    public void onEntityCombust(final EntityCombustEvent event) {
        if (event instanceof EntityCombustByEntityEvent || event instanceof EntityCombustByBlockEvent) {
            return;
        }
        if (event.getEntity() instanceof Zombie) {
            if (isNPC(event.getEntity())) {
                // Prevent undead combustion for NPCs.
                event.setCancelled(true);
            }
        }
    }

    /**
     * Checks whether the given entity is an NPC.
     *
     * @param entity The entity.
     * @return {@code true} iff the entity is an NPC.
     */
    public boolean isNPC(final Entity entity) {
        return npcDataStorage.containsKey(entity.getUniqueId());
    }

    /**
     * Fetches an NPC by UUID.
     *
     * @param id The UUID of the NPC.
     * @return The NPC.
     */
    public NPC getNPC(final UUID id) {
        final NPC npc = npcStorage.get(id);
        if (npc != null && npc.isPresent()) {
            return npc;
        }
        return null;
    }

    /**
     * Fetches an NPC by entity ID.
     *
     * @param id The entity ID of the NPC.
     * @return The NPC.
     */
    public NPC getNPC(final int id) {
        final UUID uid = npcEntityIdMap.get(id);
        if (uid == null) {
            return null;
        }

        final NPC npc = npcStorage.get(uid);
        if (npc != null && npc.isPresent()) {
            return npc;
        }
        return null;
    }

    /**
     * Checks whether an NPC with the given ID is loaded.
     *
     * @param id The id.
     * @return {@code true} iff such an NPC is loaded.
     */
    private boolean isLoaded(final UUID id) {
        final NPC npc = npcStorage.get(id);
        return npc != null && npc.isPresent();
    }

    /**
     * Collects NPCs in all worlds.
     */
    private void scrapeAndInitializeAll() {
        for (final World world : Bukkit.getWorlds()) {
            for (final LivingEntity entity : world.getLivingEntities()) {
                if (entity instanceof Zombie && isNPC(entity)) {
                    initializeNPC((Zombie) entity);
                }
            }
        }
    }

    /**
     * Initializes the given entity as an NPC. This method is idempotent. This method may only be called for NPCs.
     *
     * @param entity The entity.
     */
    private void initializeNPC(final Zombie entity) {
        // Store the entity ID in the entity ID map.
        npcEntityIdMap.put(entity.getEntityId(), entity.getUniqueId());

        // Initialize the NPC iff it is not loaded. All loaded NPCs are assumed to be initialized.
        if (!isLoaded(entity.getUniqueId())) {
            npcStorage.put(entity.getUniqueId(),
                    new NPC(new WeakReference<>(entity), npcDataStorage.get(entity.getUniqueId())));
        }

        // Ensure the NPC is not only loaded, but also the stored entities match, to be on the safe side.
        final NPC storedNPC = npcStorage.get(entity.getUniqueId());
        final NPC npc;
        if (storedNPC.getBukkitEntity() == entity) {
            // Entity match.
            npc = storedNPC;
        } else {
            // Entity mismatch! Reload the entry in the storage.
            final NPC overwriteNPC = new NPC(new WeakReference<>(entity), npcDataStorage.get(entity.getUniqueId()));
            npcStorage.put(entity.getUniqueId(), overwriteNPC);
            npc = overwriteNPC;
        }

        npc.initializeEntity();
    }
}
