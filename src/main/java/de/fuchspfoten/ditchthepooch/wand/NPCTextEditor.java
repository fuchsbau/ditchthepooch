package de.fuchspfoten.ditchthepooch.wand;

import de.fuchspfoten.ditchthepooch.DitchThePoochPlugin;
import de.fuchspfoten.ditchthepooch.modules.ZombieNPCModule;
import de.fuchspfoten.ditchthepooch.npc.NPC;
import de.fuchspfoten.fuchslib.Messenger;
import lombok.RequiredArgsConstructor;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.ConversationFactory;
import org.bukkit.conversations.Prompt;
import org.bukkit.conversations.StringPrompt;
import org.bukkit.entity.Entity;
import org.bukkit.event.Event;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.EquipmentSlot;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Changes NPC texts.
 */
public class NPCTextEditor implements Consumer<Event> {

    /**
     * A conversation factory for text prompts.
     */
    private final ConversationFactory conversationFactory;

    /**
     * Constructor.
     */
    public NPCTextEditor() {
        Messenger.register("ditchthepooch.wand.npcText.desc");
        Messenger.register("ditchthepooch.wand.npcText.menu");
        Messenger.register("ditchthepooch.wand.npcText.keyList");
        Messenger.register("ditchthepooch.wand.npcText.keyTextList");
        Messenger.register("ditchthepooch.wand.npcText.keyAdded");
        Messenger.register("ditchthepooch.wand.npcText.keyRemoved");
        Messenger.register("ditchthepooch.wand.npcText.failure");
        Messenger.register("ditchthepooch.wand.npcText.textAdded");
        Messenger.register("ditchthepooch.wand.npcText.textRemoved");
        Messenger.register("ditchthepooch.wand.npcText.noText");
        Messenger.register("ditchthepooch.wand.npcText.keyMenu");
        Messenger.register("ditchthepooch.wand.npcText.exitedMenu");

        conversationFactory = new ConversationFactory(DitchThePoochPlugin.getSelf())
                .withModality(false)
                .withLocalEcho(false)
                .withTimeout(60);
    }

    @Override
    public void accept(final Event e) {
        if (!(e instanceof PlayerInteractEntityEvent)) {
            return;
        }
        final PlayerInteractEntityEvent event = (PlayerInteractEntityEvent) e;

        if (event.getHand() != EquipmentSlot.HAND) {
            return;
        }

        final Entity clicked = event.getRightClicked();
        final ZombieNPCModule module = DitchThePoochPlugin.getSelf().getZombieNPCModule();
        if (!module.isNPC(clicked)) {
            return;
        }

        conversationFactory.withFirstPrompt(new KeyMenuPrompt(module.getNPC(clicked.getUniqueId())))
                .buildConversation(event.getPlayer())
                .begin();
    }

    /**
     * Provides a menu to edit text keys of NPCs.
     */
    @RequiredArgsConstructor
    private static class KeyMenuPrompt extends StringPrompt {

        /**
         * The target NPC.
         */
        private final NPC targetNPC;

        @Override
        public String getPromptText(final ConversationContext context) {
            return Messenger.getFormat("ditchthepooch.wand.npcText.menu");
        }

        @Override
        public Prompt acceptInput(final ConversationContext context, final String input) {
            final String[] parts = input.split(" ", 2);
            final String key = parts[0].toLowerCase();
            final CommandSender sender = (CommandSender) context.getForWhom();

            if (key.equals("list")) {
                Messenger.send(sender, "ditchthepooch.wand.npcText.keyList",
                        String.join(", ", targetNPC.getData().getTextKeys()));
            } else if (key.equals("add") && parts.length > 1) {
                if (targetNPC.getData().addTextKey(parts[1])) {
                    Messenger.send(sender, "ditchthepooch.wand.npcText.keyAdded");
                } else {
                    Messenger.send(sender, "ditchthepooch.wand.npcText.failure");
                }
            } else if (key.equals("remove") && parts.length > 1) {
                if (targetNPC.getData().removeTextKey(parts[1])) {
                    Messenger.send(sender, "ditchthepooch.wand.npcText.keyRemoved");
                } else {
                    Messenger.send(sender, "ditchthepooch.wand.npcText.failure");
                }
            } else if (key.equals("edit") && parts.length > 1) {
                if (targetNPC.getData().getTextKeys().contains(parts[1])) {
                    return new TextMenuPrompt(targetNPC, this, parts[1]);
                }
                Messenger.send(sender, "ditchthepooch.wand.npcText.failure");
            } else if (key.equals("quit")) {
                Messenger.send(sender, "ditchthepooch.wand.npcText.exitedMenu");
                return Prompt.END_OF_CONVERSATION;
            }
            return this;
        }

        @Override
        public KeyMenuPrompt clone() throws CloneNotSupportedException {
            throw new CloneNotSupportedException();
        }
    }

    /**
     * Provides a menu to edit text of NPCs.
     */
    @RequiredArgsConstructor
    private static class TextMenuPrompt extends StringPrompt {

        /**
         * The target NPC.
         */
        private final NPC targetNPC;

        /**
         * The parent prompt.
         */
        private final Prompt parent;

        /**
         * The text key to edit.
         */
        private final String key;

        @Override
        public String getPromptText(final ConversationContext context) {
            return Messenger.getFormat("ditchthepooch.wand.npcText.keyMenu");
        }

        @Override
        public Prompt acceptInput(final ConversationContext context, final String input) {
            final String[] parts = input.split(" ", 2);
            final String commandKey = parts[0].toLowerCase();
            final CommandSender sender = (CommandSender) context.getForWhom();

            if (commandKey.equals("list")) {
                final List<String> text = targetNPC.getData().getTextForKey(key);
                for (int i = 0; i < text.size(); i++) {
                    Messenger.send(sender, "ditchthepooch.wand.npcText.keyTextList", i, text.get(i));
                }
                if (text.isEmpty()) {
                    Messenger.send(sender, "ditchthepooch.wand.npcText.noText");
                }
            } else if (commandKey.equals("quit")) {
                Messenger.send(sender, "ditchthepooch.wand.npcText.exitedMenu");
                return parent;
            } else {
                final int offset;
                try {
                    offset = Integer.parseInt(commandKey);
                } catch (final NumberFormatException ex) {
                    Messenger.send(sender, "ditchthepooch.wand.npcText.failure");
                    return this;
                }

                final List<String> text = new LinkedList<>(targetNPC.getData().getTextForKey(key));
                if (parts.length == 1) {
                    // Remove.
                    if (text.size() > offset) {
                        text.remove(offset);
                        targetNPC.getData().setTextForKey(key, text);
                        Messenger.send(sender, "ditchthepooch.wand.npcText.textRemoved");
                    } else {
                        Messenger.send(sender, "ditchthepooch.wand.npcText.failure");
                    }
                } else {
                    // Add.
                    if (text.size() >= offset) {
                        text.add(offset, parts[1]);
                        targetNPC.getData().setTextForKey(key, text);
                        Messenger.send(sender, "ditchthepooch.wand.npcText.textAdded");
                    } else {
                        Messenger.send(sender, "ditchthepooch.wand.npcText.failure");
                    }
                }
            }
            return this;
        }

        @Override
        public TextMenuPrompt clone() throws CloneNotSupportedException {
            throw new CloneNotSupportedException();
        }
    }
}
