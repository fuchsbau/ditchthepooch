package de.fuchspfoten.ditchthepooch.wand;

import de.fuchspfoten.ditchthepooch.DitchThePoochPlugin;
import de.fuchspfoten.ditchthepooch.modules.ZombieNPCModule;
import de.fuchspfoten.fuchslib.Messenger;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Zombie;
import org.bukkit.event.Event;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.EquipmentSlot;

import java.util.function.Consumer;

/**
 * Converts zombies into NPCs.
 */
public class NPCCreator implements Consumer<Event> {

    /**
     * Constructor.
     */
    public NPCCreator() {
        Messenger.register("ditchthepooch.wand.npcCreate.alreadyNPC");
        Messenger.register("ditchthepooch.wand.npcCreate.notSneaking");
        Messenger.register("ditchthepooch.wand.npcCreate.npcAdded");
        Messenger.register("ditchthepooch.wand.npcCreate.desc");
    }

    @Override
    public void accept(final Event e) {
        if (!(e instanceof PlayerInteractEntityEvent)) {
            return;
        }
        final PlayerInteractEntityEvent event = (PlayerInteractEntityEvent) e;

        if (event.getHand() != EquipmentSlot.HAND) {
            return;
        }

        final Entity clicked = event.getRightClicked();
        if (!(clicked instanceof Zombie)) {
            return;
        }
        final Zombie zombie = (Zombie) clicked;

        if (!event.getPlayer().isSneaking()) {
            Messenger.send(event.getPlayer(), "ditchthepooch.wand.npcCreate.notSneaking");
            return;
        }

        final ZombieNPCModule npcModule = DitchThePoochPlugin.getSelf().getZombieNPCModule();
        if (npcModule.isNPC(zombie)) {
            Messenger.send(event.getPlayer(), "ditchthepooch.wand.npcCreate.alreadyNPC");
            return;
        }

        npcModule.addNPC(zombie);
        Messenger.send(event.getPlayer(), "ditchthepooch.wand.npcCreate.npcAdded");
    }
}
