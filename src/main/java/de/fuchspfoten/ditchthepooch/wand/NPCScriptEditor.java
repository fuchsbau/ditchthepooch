package de.fuchspfoten.ditchthepooch.wand;

import de.fuchspfoten.ditchthepooch.DitchThePoochPlugin;
import de.fuchspfoten.ditchthepooch.modules.ZombieNPCModule;
import de.fuchspfoten.ditchthepooch.npc.NPC;
import de.fuchspfoten.fuchslib.Messenger;
import lombok.RequiredArgsConstructor;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.ConversationFactory;
import org.bukkit.conversations.Prompt;
import org.bukkit.conversations.StringPrompt;
import org.bukkit.entity.Entity;
import org.bukkit.event.Event;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.EquipmentSlot;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Changes NPC scripts.
 */
public class NPCScriptEditor implements Consumer<Event> {

    /**
     * A conversation factory for script prompts.
     */
    private final ConversationFactory conversationFactory;

    /**
     * Constructor.
     */
    public NPCScriptEditor() {
        Messenger.register("ditchthepooch.wand.npcScript.desc");
        Messenger.register("ditchthepooch.wand.npcScript.menu");
        Messenger.register("ditchthepooch.wand.npcScript.triggerList");
        Messenger.register("ditchthepooch.wand.npcScript.triggerScriptList");
        Messenger.register("ditchthepooch.wand.npcScript.triggerAdded");
        Messenger.register("ditchthepooch.wand.npcScript.triggerRemoved");
        Messenger.register("ditchthepooch.wand.npcScript.failure");
        Messenger.register("ditchthepooch.wand.npcScript.scriptAdded");
        Messenger.register("ditchthepooch.wand.npcScript.scriptRemoved");
        Messenger.register("ditchthepooch.wand.npcScript.noScript");
        Messenger.register("ditchthepooch.wand.npcScript.triggerMenu");
        Messenger.register("ditchthepooch.wand.npcScript.exitedMenu");

        conversationFactory = new ConversationFactory(DitchThePoochPlugin.getSelf())
                .withModality(false)
                .withLocalEcho(false)
                .withTimeout(60);
    }

    @Override
    public void accept(final Event e) {
        if (!(e instanceof PlayerInteractEntityEvent)) {
            return;
        }
        final PlayerInteractEntityEvent event = (PlayerInteractEntityEvent) e;

        if (event.getHand() != EquipmentSlot.HAND) {
            return;
        }

        final Entity clicked = event.getRightClicked();
        final ZombieNPCModule module = DitchThePoochPlugin.getSelf().getZombieNPCModule();
        if (!module.isNPC(clicked)) {
            return;
        }

        conversationFactory.withFirstPrompt(new TriggerMenuPrompt(module.getNPC(clicked.getUniqueId())))
                .buildConversation(event.getPlayer())
                .begin();
    }

    /**
     * Provides a menu to edit triggers of NPCs.
     */
    @RequiredArgsConstructor
    private static class TriggerMenuPrompt extends StringPrompt {

        /**
         * The target NPC.
         */
        private final NPC targetNPC;

        @Override
        public String getPromptText(final ConversationContext context) {
            return Messenger.getFormat("ditchthepooch.wand.npcScript.menu");
        }

        @Override
        public Prompt acceptInput(final ConversationContext context, final String input) {
            final String[] parts = input.split(" ", 2);
            final String key = parts[0].toLowerCase();
            final CommandSender sender = (CommandSender) context.getForWhom();

            if (key.equals("list")) {
                Messenger.send(sender, "ditchthepooch.wand.npcScript.triggerList",
                        String.join(", ", targetNPC.getData().getTriggers()));
            } else if (key.equals("add") && parts.length > 1) {
                if (targetNPC.getData().addTrigger(parts[1])) {
                    Messenger.send(sender, "ditchthepooch.wand.npcScript.triggerAdded");
                } else {
                    Messenger.send(sender, "ditchthepooch.wand.npcScript.failure");
                }
            } else if (key.equals("remove") && parts.length > 1) {
                if (targetNPC.getData().removeTrigger(parts[1])) {
                    Messenger.send(sender, "ditchthepooch.wand.npcScript.triggerRemoved");
                } else {
                    Messenger.send(sender, "ditchthepooch.wand.npcScript.failure");
                }
            } else if (key.equals("edit") && parts.length > 1) {
                if (targetNPC.getData().getTriggers().contains(parts[1])) {
                    return new ScriptMenuPrompt(targetNPC, this, parts[1]);
                }
                Messenger.send(sender, "ditchthepooch.wand.npcScript.failure");
            } else if (key.equals("quit")) {
                Messenger.send(sender, "ditchthepooch.wand.npcScript.exitedMenu");
                return Prompt.END_OF_CONVERSATION;
            }
            return this;
        }

        @Override
        public TriggerMenuPrompt clone() throws CloneNotSupportedException {
            throw new CloneNotSupportedException();
        }
    }

    /**
     * Provides a menu to edit scripts of NPCs.
     */
    @RequiredArgsConstructor
    private static class ScriptMenuPrompt extends StringPrompt {

        /**
         * The target NPC.
         */
        private final NPC targetNPC;

        /**
         * The parent prompt.
         */
        private final Prompt parent;

        /**
         * The trigger to edit.
         */
        private final String trigger;

        @Override
        public String getPromptText(final ConversationContext context) {
            return Messenger.getFormat("ditchthepooch.wand.npcScript.triggerMenu");
        }

        @Override
        public Prompt acceptInput(final ConversationContext context, final String input) {
            final String[] parts = input.split(" ", 2);
            final String commandKey = parts[0].toLowerCase();
            final CommandSender sender = (CommandSender) context.getForWhom();

            if (commandKey.equals("list")) {
                final List<String> script = targetNPC.getData().getTriggerScript(trigger);
                for (int i = 0; i < script.size(); i++) {
                    Messenger.send(sender, "ditchthepooch.wand.npcScript.triggerScriptList", i, script.get(i));
                }
                if (script.isEmpty()) {
                    Messenger.send(sender, "ditchthepooch.wand.npcScript.noScript");
                }
            } else if (commandKey.equals("quit")) {
                Messenger.send(sender, "ditchthepooch.wand.npcScript.exitedMenu");
                return parent;
            } else {
                final int offset;
                try {
                    offset = Integer.parseInt(commandKey);
                } catch (final NumberFormatException ex) {
                    Messenger.send(sender, "ditchthepooch.wand.npcScript.failure");
                    return this;
                }

                final List<String> script = new LinkedList<>(targetNPC.getData().getTriggerScript(trigger));
                if (parts.length == 1) {
                    // Remove.
                    if (script.size() > offset) {
                        script.remove(offset);
                        targetNPC.getData().setTriggerScript(trigger, script);
                        Messenger.send(sender, "ditchthepooch.wand.npcScript.scriptRemoved");
                    } else {
                        Messenger.send(sender, "ditchthepooch.wand.npcScript.failure");
                    }
                } else {
                    // Add.
                    if (script.size() >= offset) {
                        script.add(offset, parts[1]);
                        targetNPC.getData().setTriggerScript(trigger, script);
                        Messenger.send(sender, "ditchthepooch.wand.npcScript.scriptAdded");
                    } else {
                        Messenger.send(sender, "ditchthepooch.wand.npcScript.failure");
                    }
                }
            }
            return this;
        }

        @Override
        public ScriptMenuPrompt clone() throws CloneNotSupportedException {
            throw new CloneNotSupportedException();
        }
    }
}
