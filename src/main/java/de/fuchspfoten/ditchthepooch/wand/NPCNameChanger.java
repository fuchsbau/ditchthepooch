package de.fuchspfoten.ditchthepooch.wand;

import de.fuchspfoten.ditchthepooch.DitchThePoochPlugin;
import de.fuchspfoten.ditchthepooch.modules.ZombieNPCModule;
import de.fuchspfoten.ditchthepooch.npc.NPC;
import de.fuchspfoten.fuchslib.Messenger;
import lombok.RequiredArgsConstructor;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.ConversationFactory;
import org.bukkit.conversations.Prompt;
import org.bukkit.conversations.StringPrompt;
import org.bukkit.entity.Entity;
import org.bukkit.event.Event;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.EquipmentSlot;

import java.util.function.Consumer;

/**
 * Changes NPC names.
 */
public class NPCNameChanger implements Consumer<Event> {

    /**
     * A conversation factory for name prompts.
     */
    private final ConversationFactory conversationFactory;

    /**
     * Constructor.
     */
    public NPCNameChanger() {
        Messenger.register("ditchthepooch.wand.npcName.query");
        Messenger.register("ditchthepooch.wand.npcName.done");
        Messenger.register("ditchthepooch.wand.npcName.desc");

        conversationFactory = new ConversationFactory(DitchThePoochPlugin.getSelf())
                .withModality(false)
                .withLocalEcho(false)
                .withTimeout(60)
                .addConversationAbandonedListener(
                        c -> Messenger.send((CommandSender) c.getContext().getForWhom(),
                                "ditchthepooch.wand.npcName.done"));
    }

    @Override
    public void accept(final Event e) {
        if (!(e instanceof PlayerInteractEntityEvent)) {
            return;
        }
        final PlayerInteractEntityEvent event = (PlayerInteractEntityEvent) e;

        if (event.getHand() != EquipmentSlot.HAND) {
            return;
        }

        final Entity clicked = event.getRightClicked();
        final ZombieNPCModule module = DitchThePoochPlugin.getSelf().getZombieNPCModule();
        if (!module.isNPC(clicked)) {
            return;
        }

        conversationFactory.withFirstPrompt(new NamePrompt(module.getNPC(clicked.getUniqueId())))
                .buildConversation(event.getPlayer())
                .begin();
    }

    /**
     * Accepts an NPC name.
     */
    @RequiredArgsConstructor
    private static class NamePrompt extends StringPrompt {

        /**
         * The target NPC.
         */
        private final NPC targetNPC;

        @Override
        public String getPromptText(final ConversationContext context) {
            return Messenger.getFormat("ditchthepooch.wand.npcName.query");
        }

        @Override
        public Prompt acceptInput(final ConversationContext context, final String input) {
            if (!input.isEmpty()) {
                targetNPC.getData().setName(ChatColor.translateAlternateColorCodes('&', input));
            }
            return Prompt.END_OF_CONVERSATION;
        }

        @Override
        public NamePrompt clone() throws CloneNotSupportedException {
            throw new CloneNotSupportedException();
        }
    }
}
