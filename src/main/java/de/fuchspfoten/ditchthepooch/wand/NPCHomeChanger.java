package de.fuchspfoten.ditchthepooch.wand;

import de.fuchspfoten.ditchthepooch.DitchThePoochPlugin;
import de.fuchspfoten.ditchthepooch.modules.ZombieNPCModule;
import de.fuchspfoten.ditchthepooch.npc.NPC;
import de.fuchspfoten.fuchslib.Messenger;
import lombok.RequiredArgsConstructor;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.ConversationFactory;
import org.bukkit.conversations.Prompt;
import org.bukkit.conversations.StringPrompt;
import org.bukkit.entity.Entity;
import org.bukkit.event.Event;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.EquipmentSlot;

import java.util.function.Consumer;

/**
 * Changes NPC homes.
 */
public class NPCHomeChanger implements Consumer<Event> {

    /**
     * A conversation factory for home prompts.
     */
    private final ConversationFactory conversationFactory;

    /**
     * Constructor.
     */
    public NPCHomeChanger() {
        Messenger.register("ditchthepooch.wand.npcHome.query");
        Messenger.register("ditchthepooch.wand.npcHome.done");
        Messenger.register("ditchthepooch.wand.npcHome.desc");

        conversationFactory = new ConversationFactory(DitchThePoochPlugin.getSelf())
                .withModality(false)
                .withLocalEcho(false)
                .withEscapeSequence("quit")
                .withTimeout(60);
    }

    @Override
    public void accept(final Event e) {
        if (!(e instanceof PlayerInteractEntityEvent)) {
            return;
        }
        final PlayerInteractEntityEvent event = (PlayerInteractEntityEvent) e;

        if (event.getHand() != EquipmentSlot.HAND) {
            return;
        }

        final Entity clicked = event.getRightClicked();
        final ZombieNPCModule module = DitchThePoochPlugin.getSelf().getZombieNPCModule();
        if (!module.isNPC(clicked)) {
            return;
        }

        conversationFactory.withFirstPrompt(new HomePrompt(module.getNPC(clicked.getUniqueId())))
                .buildConversation(event.getPlayer())
                .begin();
    }

    /**
     * Accepts an NPC home (by checking the player's location).
     */
    @RequiredArgsConstructor
    private static class HomePrompt extends StringPrompt {

        /**
         * The target NPC.
         */
        private final NPC targetNPC;

        @Override
        public String getPromptText(final ConversationContext context) {
            return Messenger.getFormat("ditchthepooch.wand.npcHome.query");
        }

        @Override
        public Prompt acceptInput(final ConversationContext context, final String input) {
            targetNPC.getData().setHomeLocation(((Entity) context.getForWhom()).getLocation());
            Messenger.send((CommandSender) context.getForWhom(), "ditchthepooch.wand.npcHome.done");
            return Prompt.END_OF_CONVERSATION;
        }

        @Override
        public HomePrompt clone() throws CloneNotSupportedException {
            throw new CloneNotSupportedException();
        }
    }
}
