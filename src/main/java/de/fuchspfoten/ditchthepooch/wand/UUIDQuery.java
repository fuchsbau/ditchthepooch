package de.fuchspfoten.ditchthepooch.wand;

import de.fuchspfoten.fuchslib.Messenger;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.player.PlayerEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.EquipmentSlot;

import java.util.Arrays;
import java.util.function.Consumer;

/**
 * Querys entities for their UUID.
 */
public class UUIDQuery implements Consumer<Event> {

    /**
     * Constructor.
     */
    public UUIDQuery() {
        Messenger.register("ditchthepooch.wand.uuid.text");
        Messenger.register("ditchthepooch.wand.uuid.desc");
    }

    @Override
    public void accept(final Event event) {
        if (!(event instanceof PlayerInteractEntityEvent)) {
            return;
        }
        if (((PlayerInteractEntityEvent) event).getHand() != EquipmentSlot.HAND) {
            return;
        }
        final Entity clicked = ((PlayerInteractEntityEvent) event).getRightClicked();
        final Player issuer = ((PlayerEvent) event).getPlayer();
        final BaseComponent[] messages = TextComponent.fromLegacyText(
                Messenger.getFormat("ditchthepooch.wand.uuid.text"));
        Arrays.stream(messages).forEach(x -> x.setClickEvent(
                new ClickEvent(Action.SUGGEST_COMMAND, clicked.getUniqueId().toString())));
        issuer.spigot().sendMessage(messages);
    }
}
