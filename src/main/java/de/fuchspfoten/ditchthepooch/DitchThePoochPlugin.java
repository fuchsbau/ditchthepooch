package de.fuchspfoten.ditchthepooch;

import com.comphenix.protocol.ProtocolLibrary;
import de.fuchspfoten.ditchthepooch.modules.BlockExplosionBlockDamageModule;
import de.fuchspfoten.ditchthepooch.modules.BreedingDisablerModule;
import de.fuchspfoten.ditchthepooch.modules.VillagerInvincibilityModule;
import de.fuchspfoten.ditchthepooch.modules.ZombieNPCModule;
import de.fuchspfoten.ditchthepooch.npc.NPCData;
import de.fuchspfoten.ditchthepooch.npc.NPCFilter;
import de.fuchspfoten.ditchthepooch.wand.NPCCreator;
import de.fuchspfoten.ditchthepooch.wand.NPCHomeChanger;
import de.fuchspfoten.ditchthepooch.wand.NPCNameChanger;
import de.fuchspfoten.ditchthepooch.wand.NPCScriptEditor;
import de.fuchspfoten.ditchthepooch.wand.NPCTextEditor;
import de.fuchspfoten.ditchthepooch.wand.UUIDQuery;
import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.harald.EditingWand;
import de.fuchspfoten.harald.EditingWandRegistration;
import lombok.Getter;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

/**
 * The main plugin class.
 */
public class DitchThePoochPlugin extends JavaPlugin {

    /**
     * The singleton plugin instance.
     */
    private @Getter static DitchThePoochPlugin self;

    /**
     * The zombie NPC module.
     */
    private @Getter ZombieNPCModule zombieNPCModule;

    @Override
    public void onDisable() {
        zombieNPCModule.save();
    }

    @Override
    public void onEnable() {
        self = this;

        // Register serializable classes.
        ConfigurationSerialization.registerClass(NPCData.class);

        // Register messages.
        Messenger.register("ditchthepooch.npc.talk");

        // Create the default configuration.
        getConfig().options().copyDefaults(true);
        saveConfig();

        // Create data directories.
        final File npcDir = new File(getDataFolder(), "npcs");
        if (!npcDir.isDirectory()) {
            if (!npcDir.mkdir()) {
                throw new IllegalStateException("could not create npc directory");
            }
        }

        // Register command executor modules.
        // ...

        // Register modules.
        getServer().getPluginManager().registerEvents(new BlockExplosionBlockDamageModule(), this);
        getServer().getPluginManager().registerEvents(new BreedingDisablerModule(), this);
        getServer().getPluginManager().registerEvents(new VillagerInvincibilityModule(), this);
        zombieNPCModule = new ZombieNPCModule(npcDir);
        getServer().getPluginManager().registerEvents(zombieNPCModule, this);

        // Register wand tools.
        EditingWand.register(new EditingWandRegistration(new UUIDQuery(), "ditchthepooch.wand.uuid.desc"));
        EditingWand.register(new EditingWandRegistration(new NPCCreator(),
                "ditchthepooch.wand.npcCreate.desc"));
        EditingWand.register(new EditingWandRegistration(new NPCNameChanger(),
                "ditchthepooch.wand.npcName.desc"));
        EditingWand.register(new EditingWandRegistration(new NPCHomeChanger(),
                "ditchthepooch.wand.npcHome.desc"));
        EditingWand.register(new EditingWandRegistration(new NPCTextEditor(),
                "ditchthepooch.wand.npcText.desc"));
        EditingWand.register(new EditingWandRegistration(new NPCScriptEditor(),
                "ditchthepooch.wand.npcScript.desc"));

        // Register protocol filters.
        ProtocolLibrary.getProtocolManager().addPacketListener(new NPCFilter(zombieNPCModule));
    }
}
