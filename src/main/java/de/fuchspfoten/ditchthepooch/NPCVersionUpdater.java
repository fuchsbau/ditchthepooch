package de.fuchspfoten.ditchthepooch;

import de.fuchspfoten.fuchslib.data.DataFile;

import java.util.function.BiConsumer;

/**
 * Updates versions of NPC data.
 */
public class NPCVersionUpdater implements BiConsumer<Integer, DataFile> {

    /**
     * The current data file version.
     */
    public static final int VERSION = 1;

    @Override
    public void accept(final Integer integer, final DataFile dataFile) {
        // Do nothing.
    }
}
